﻿using UnityEngine;
using System.Collections;

public class History : MonoBehaviour {
	public static GameObject HistoryShow;
	public static GameObject Title;
	public static GameObject Content;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick() {
		if (HistoryShow == null) {
			HistoryShow = Vuforia.DefaultTrackableEventHandler.FindourStuff("HistoryShow");
		}
		HistoryShow.SetActive (true);
		if (Title == null) {
			Title = GameObject.Find ("HistoryTitle");
		}
		if (Content == null) {
			Content = GameObject.Find ("HistoryContent");
		}
		Title.GetComponent<HistoryTitle> ().Show ();
		Content.GetComponent<HistoryContent> ().Show ();
		Vuforia.DefaultTrackableEventHandler.ourMenu.SetActive (false);
	}

	public void OnClickExit() {
		Title.GetComponent<HistoryTitle> ().Hide ();
		Content.GetComponent<HistoryContent> ().Hide ();
		HistoryShow.SetActive (false);
		Vuforia.DefaultTrackableEventHandler.ourMenu.SetActive (true);
	}
}
