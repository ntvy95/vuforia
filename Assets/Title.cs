﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Title : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Show() {
		GetComponent<Text>().text = Vuforia.DefaultTrackableEventHandler.current.name;
	}

	public void Hide() {
		GetComponent<Text>().text = "";
	}
}
