﻿using UnityEngine;
using System.Collections;

//Below are trivial comments made when being stressed. Never mind.
//Unity itself probably is not for gallery or any stuff that needs just-right layout.
//I guess that is why I hate it. My GUI strength lies in stuffs that has proper layout and can be controlled properly by code not by hands nor by eyes.
//Heard that eDriven GUI has that job done nicely. Wow, but it does not look like that I have time to play around it now, so let do simple things first.

public class Gallery : MonoBehaviour {
	public static GameObject rootcanvas;
	public static float maxwidth;
	public static RectTransform[] childrect;
	public static GameObject GalleryShow;
	public static GameObject ourGallery;
	// Use this for initialization
	void Start () {
		if (name != "EventSystem") {
			if(rootcanvas == null) {
				rootcanvas = GameObject.Find ("Canvas");
				maxwidth = rootcanvas.GetComponent<RectTransform> ().sizeDelta.x;
			}
			//SIMPLY NOT BEING ABLE TO UNDERSTAND HOW LOCALPOSITION WORKS!!!
			//Little note:
			//The localPosition is not as simple as it seems, it may be changes way of displaying due to the width and height of the parent object.
			//Be careful.

			childrect = GetComponentsInChildren<RectTransform> ();
			this.setimage();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (name != "EventSystem" && maxwidth != rootcanvas.GetComponent<RectTransform> ().sizeDelta.x) {
			maxwidth = rootcanvas.GetComponent<RectTransform> ().sizeDelta.x;
			this.setimage();
		}
	}

	public void setimage() {
		setmaxwidth (childrect, maxwidth);
		this.setimagedistance (childrect, 50f);
		
		RectTransform childmin = childrect [childrect.Length - 1];
		float height = (float)(childmin.sizeDelta.y - childmin.localPosition.y);
		GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, height);
		this.setimagedistance (childrect, 50f);
	}
		
	public void OnClick() {
		if(GalleryShow == null) {
			GalleryShow = Vuforia.DefaultTrackableEventHandler.FindourStuff ("GalleryShow");
		}
		GalleryShow.SetActive (true);
		ourGallery = Vuforia.DefaultTrackableEventHandler.FindourStuff(Vuforia.DefaultTrackableEventHandler.current.tag + "-galleryscroll");
		ourGallery.SetActive (true);
		Vuforia.DefaultTrackableEventHandler.ourMenu.SetActive (false);
	}

	public void OnClickExit() {
		ourGallery.SetActive (false);
		GalleryShow.SetActive (false);
		Vuforia.DefaultTrackableEventHandler.ourMenu.SetActive (true);
	}

	public static void setmaxwidth(RectTransform[] collection, float mw) {
		for (int i = 1; i < collection.Length; i++) {
			RectTransform item = collection[i];
			float ratio = mw / item.sizeDelta.x;
			float height = ratio * item.sizeDelta.y;
			item.sizeDelta = new Vector2(mw, height);
		}
	}

	public void setimagedistance(RectTransform[] collection, float distance) {
		//ASSUMPTION: They are in the right order, meaning that the below has y-axis value lower than the above.
		float currenty = 0;
		for(int i = 1; i < collection.Length; i++) {
			RectTransform item = collection[i];
			item.localPosition = new Vector3(0, currenty, item.localPosition.z);
			currenty = currenty - item.sizeDelta.y - distance;
		}
	}
}
