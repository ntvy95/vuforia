﻿using UnityEngine;
using System.Collections;

public class AboutThis : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick() {
		if (Vuforia.DefaultTrackableEventHandler.current.tag == "ourCanvas") {
			Application.OpenURL ("http://uit.edu.vn");
		} else {
			Application.OpenURL ("http://google.com.vn");
		}
	}
}
