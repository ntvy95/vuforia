﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HistoryContent : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show() {
		if (Vuforia.DefaultTrackableEventHandler.current.name == "University of Information Technology") {
			GetComponent<Text>().text = "The predecessor of this university was Center for IT Development. On 8 June 2006, the Vietnamese prime minister signed Decision no. 134/2006/QĐ-TTg to establish this university.";
		}
	}

	public void Hide() {
		GetComponent<Text>().text = "";
	}
}
